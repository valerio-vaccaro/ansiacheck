from flask import Flask, flash, redirect, render_template, request, session, abort, escape, send_file
from flask_stache import render_view, render_template
from flask_qrcode import QRcode
from math import *
import os
import configparser
import json
import mysql.connector
import datetime


elementsPerPage = 100
version = 'v.0.0.1'

app = Flask(__name__, static_url_path='/static')
qrcode = QRcode(app)

config = configparser.RawConfigParser()
config.read('ansiacheck.conf')

myHost = config.get('MYSQL', 'host')
myUser = config.get('MYSQL', 'username')
myPasswd = config.get('MYSQL', 'password')
myDatabase = config.get('MYSQL', 'database')


@app.route('/', methods = ['GET'])
def home():
    page = request.args.get('page', default = 0, type = int)
    mydb = mysql.connector.connect(host=myHost, user=myUser, passwd=myPasswd, database=myDatabase)
    mycursor = mydb.cursor()

    sql = 'SELECT id, title, summary, link, json, hash, ots, ots_status FROM news ORDER BY id DESC LIMIT ' + str(elementsPerPage) + ' OFFSET ' + str(elementsPerPage*page)
    mycursor.execute(sql)

    results = []
    for myrow in mycursor:
        row = {
            'id': myrow[0],
            'title': myrow[1],
            'summary': myrow[2],
            'link': myrow[3],
            'json': myrow[4],
            'hash': myrow[5],
            'ots': myrow[6],
            'ots_status': myrow[7],
        }
        results.append(row)

    mycursor.execute('SELECT count(*) FROM news')
    links = [{'id':x} for x in range(0, ceil(mycursor.fetchone()[0]/elementsPerPage))]

    mydb.close()
    data = {
        'results': results,
        'version': version,
        'links': links,
    }
    return render_template('home', **data)

@app.route('/about', methods = ['GET'])
def about():
    data = {
        'results': '',
        'version': version,
    }
    return render_template('about', **data)

@app.route('/news/<hash>', methods = ['GET'])
def news(hash):
    mydb = mysql.connector.connect(host=myHost, user=myUser, passwd=myPasswd, database=myDatabase)
    mycursor = mydb.cursor()

    sql = 'SELECT id, title, summary, link, json, hash, ots, ots_status FROM news WHERE hash="' + hash + '"'
    mycursor.execute(sql)

    results = []
    for myrow in mycursor:
        (pre, post) =  myrow[3].split('://')
        safelink = "https://"+post
        row = {
            'id': myrow[0],
            'title': myrow[1],
            'summary': myrow[2],
            'link': safelink,
            'json': myrow[4],
            'hash': myrow[5],
            'ots': myrow[6],
            'ots_status': myrow[7],
        }
        results.append(row)

    mydb.close()
    data = {
        'results': results,
        'version': version,
    }
    return render_template('news', **data)

@app.route('/news/<hash>/ots', methods = ['GET'])
def ots(hash):
    mydb = mysql.connector.connect(host=myHost, user=myUser, passwd=myPasswd, database=myDatabase)
    mycursor = mydb.cursor()

    sql = 'SELECT id, title, summary, link, json, hash, ots, ots_status FROM news WHERE hash="' + hash + '"'
    mycursor.execute(sql)

    results = []
    for myrow in mycursor:
        row = {
            'id': myrow[0],
            'title': myrow[1],
            'summary': myrow[2],
            'link': myrow[3],
            'json': myrow[4],
            'hash': myrow[5],
            'ots': myrow[6],
            'ots_status': myrow[7],
        }
        results.append(row)

    mydb.close()

    if results[0]['ots'] is '':
        return "No ots data present"
    else:
        ots_path = './static/ots/'+results[0]['hash']+'.ots'
        ots_file = open(ots_path, "wb")
        ots_file.write(bytearray.fromhex(results[0]['ots']))
        ots_file.close()
        return send_file(ots_path, as_attachment=True)

@app.route('/news/<hash>/json', methods = ['GET'])
def json(hash):
    mydb = mysql.connector.connect(host=myHost, user=myUser, passwd=myPasswd, database=myDatabase)
    mycursor = mydb.cursor()

    sql = 'SELECT id, title, summary, link, json, hash, ots, ots_status FROM news WHERE hash="' + hash + '"'
    mycursor.execute(sql)

    results = []
    for myrow in mycursor:
        row = {
            'id': myrow[0],
            'title': myrow[1],
            'summary': myrow[2],
            'link': myrow[3],
            'json': myrow[4],
            'hash': myrow[5],
            'ots': myrow[6],
            'ots_status': myrow[7],
        }
        results.append(row)

    mydb.close()

    if results[0]['json'] is '':
        return "No json data present"
    else:
        ots_path = './static/json/'+results[0]['hash']+'.json'
        ots_file = open(ots_path, "w")
        ots_file.write(results[0]['json'])
        ots_file.close()
        return send_file(ots_path, as_attachment=True)

if __name__ == '__main__':
    app.import_name = '.'
    app.secret_key = os.urandom(12)
    #app.debug = True
    app.run(host='0.0.0.0', port=5055)
