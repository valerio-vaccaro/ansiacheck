#!/usr/bin/env python

import json
import configparser
import mysql.connector
import re
from random import SystemRandom
from datetime import date, datetime, timedelta
import feedparser
import hashlib
import os
import wallycore as wally

config = configparser.RawConfigParser()
config.read('ansiacheck.conf')

myHost = config.get('MYSQL', 'host')
myUser = config.get('MYSQL', 'username')
myPasswd = config.get('MYSQL', 'password')
myDatabase = config.get('MYSQL', 'database')

print('---- collect news ------------------')
# Collect
rss_urls=[
    'http://feeds.bbci.co.uk/news/rss.xml',
    'https://www.ansa.it/sito/ansait_rss.xml',
    'https://www.ansa.it/sito/notizie/cronaca/cronaca_rss.xml',
    'https://www.ansa.it/sito/notizie/politica/politica_rss.xml',
    'https://www.ansa.it/sito/notizie/mondo/mondo_rss.xml',
    'https://www.ansa.it/sito/notizie/economia/economia_rss.xml',
    'https://www.ansa.it/sito/notizie/sport/calcio/calcio_rss.xml',
    'https://www.ansa.it/sito/notizie/sport/sport_rss.xml',
    'https://www.ansa.it/sito/notizie/cultura/cinema/cinema_rss.xml',
    'https://www.ansa.it/sito/notizie/cultura/cultura_rss.xml',
    'https://www.ansa.it/sito/notizie/tecnologia/tecnologia_rss.xml',
    'https://www.ansa.it/sito/notizie/topnews/topnews_rss.xml',
    'https://www.ansa.it/english/english_rss.xml',
    'https://www.ansa.it/sito/photogallery/foto_rss.xml',
    'https://www.ansa.it/sito/videogallery/video_rss.xml',
    'https://www.ansa.it/canale_ambiente/notizie/ambiente_rss.xml',
    'https://www.ansa.it/canale_motori/notizie/motori_rss.xml',
    'https://www.ansa.it/canale_terraegusto/notizie/terraegusto_rss.xml',
    'https://www.ansa.it/canale_saluteebenessere/notizie/saluteebenessere_rss.xml',
    'https://www.ansa.it/canale_scienza_tecnica/notizie/scienzaetecnica_rss.xml',
    'https://www.ansa.it/nuova_europa/it/rss.xml',
    'https://www.ansa.it/nuova_europa/en/rss.xml',
    'https://www.ansa.it/canale_viaggiart/it/notizie/viaggiart_rss.xml',
    'https://www.ansa.it/canale_lifestyle/notizie/lifestyle_rss.xml',
]

mydb = mysql.connector.connect(host=myHost, user=myUser, passwd=myPasswd, database=myDatabase)
mycursor = mydb.cursor()
for rss_url in rss_urls:
    news = feedparser.parse(rss_url)
    for article in news.entries:
        title = article['title']
        summary = article['summary']
        link = article['link']
        json_str = json.dumps({'title': title, 'summary': summary, 'date': article['published'], 'link': link})
        hash = hashlib.sha256(json_str.encode()).hexdigest()

        sql = 'SELECT COUNT(*) FROM news WHERE link="' + link + '"'
        mycursor.execute(sql)
        result = str(mycursor.fetchone())
        if result == "(0,)":
             print(link)
             sql = 'INSERT IGNORE INTO news (title, summary, link, json, hash) VALUES (%s, %s, %s, %s, %s)'
             val = (title, summary, link, json_str, hash)
             mycursor.execute(sql,val)
             mydb.commit()

print('---- new ots ------------------')
sql = 'SELECT id, hash, ots FROM news WHERE ots_status=0'
mycursor.execute(sql)
results = [{'id':r[0], 'hash':r[1], 'ots':r[2]} for r in mycursor]

for result in results:
    # new ots
    os.system('/usr/local/bin/ots-cli.js stamp -d ' + result['hash'])

    print(result['hash'] + ' create new ots')

    # opent results
    ots_path = result['hash']+'.ots'
    ots_file = open(ots_path, "rb")
    ots_hex = wally.hex_from_bytes(ots_file.read())
    ots_file.close()
    os.system('rm ' + result['hash'] + '.ots')

    # update
    sql = 'UPDATE news SET ots="' + ots_hex + '", ots_status=1 WHERE id=' + str(result['id'])
    mycursor.execute(sql)
    mydb.commit()

print('---- update ots ------------------')
# check and upgrade if needed
sql = 'SELECT id, hash, ots FROM news WHERE ots_status=1'
mycursor.execute(sql)
results = [{'id':r[0], 'hash':r[1], 'ots':r[2]} for r in mycursor]

for result in results:
    print(result['hash'] + ' try to update')

    # save file
    ots_path = result['hash']+'.ots'
    ots_file = open(ots_path, "wb")
    ots_file.write(wally.hex_to_bytes(result['ots']))
    ots_file.close()

    # upgrade ots
    os.system('/usr/local/bin/ots-cli.js upgrade ' + result['hash'] + '.ots')

    # opent results
    ots_path = result['hash']+'.ots'
    ots_file = open(ots_path, "rb")
    ots_upgraded = wally.hex_from_bytes(ots_file.read())
    ots_file.close()
    os.system('rm ' + result['hash'] + '.ots')
    os.system('rm ' + result['hash'] + '.ots.bak')

    if len(ots_upgraded) > len(result['ots']):
        # update
        sql = 'UPDATE news SET ots="' + ots_upgraded + '", ots_status=2 WHERE id=' + str(result['id'])
        mycursor.execute(sql)
        mydb.commit()

mydb.close()
